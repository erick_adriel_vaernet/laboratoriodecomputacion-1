#include <stdio.h>
#include <conio.h>
#define n 3

struct estructura_alumno {	/* Definimos la estructura estructura_alumno */
	char dni[10];
	char apellido[30];
	char nombre[30];
	int edad [4];
	int promedio [4];
};

double promedio(struct estructura_alumno a[n]);
int mayor_edad(struct estructura_alumno a[n]);
int menor_edad(struct estructura_alumno a[n]);
int nota_promedio(struct estructura_alumno a[n]);
int prom_mayor_igual_7(struct estructura_alumno a[n]);
int prom_menor_7(struct estructura_alumno a[n]);
double porcent_mayor_igual_7(int cantidadMayor7);
double porcent_menor_7(int cantidadMenor7);

int main()
{
	int i, mayorE, menorE, notaPromedio, cantMayor7, cantMenor7;
	double prom,porcentMayor7, porcentMenor7;
	struct estructura_alumno alumno[n];
	
	for(i=0; i<n; i++)
	{
		printf( "\nEscriba el DNI del alumno: " );
		scanf( "%s", &alumno[i].dni );
		printf( "Escriba el apellido del alumno: " );
		scanf( "%s", &alumno[i].apellido );
		printf( "Escriba el nombre del alumno: " );
		scanf( "%s", &alumno[i].nombre);
		printf( "Escribe la edad del alumno: " );
		scanf( "%d", &alumno[i].edad );
		printf( "Escribe el promedio del alumno: " );
		scanf( "%d", &alumno[i].promedio );
	}
	printf("\n\t\t\tListado de alumnos\n");
	printf("DNI\t\tApellido\tNombre\t\tEdad\tPromedio\n");
	
	for(i=0; i<n; i++){
		printf("%s\t\t%s\t\t%s\t\t%d\t%d\n",alumno[i].dni,alumno[i].apellido,alumno[i].nombre, *alumno[i].edad, *alumno[i].promedio);
	}

	printf("\nTotal de alumnos: %d",n);
	
	prom = promedio(alumno);
	printf("\nEl promedio de edad de los alumnos es: %.2f", prom);
	
	mayorE = mayor_edad(alumno);
	printf("\nLa mayor edad dentro de los alumnos es de %d a�os", mayorE);
	
	menorE = menor_edad(alumno);
	printf("\nLa menor edad dentro de los alumnos es de %d a�os", menorE);
	
	notaPromedio = nota_promedio(alumno);
	cantMayor7 = prom_mayor_igual_7(alumno);
	cantMenor7 = prom_menor_7(alumno);
	printf("\nLa nota promedio es de %d, habiendo %d alumno/s con una nota mayor o igual a 7 y %d alumno/s con una menor a 7 ",notaPromedio,cantMayor7,cantMenor7);
	
	porcentMayor7 = porcent_mayor_igual_7(cantMayor7);
	porcentMenor7 = porcent_menor_7(cantMenor7);
	printf("\nExiste un %.2f%% de alumnos con promedio mayor o igual a 7 y un %.2f%% de alumnos con promedio menor a 7  ",porcentMayor7,porcentMenor7);
	
	
	getch();
	return 0;
}

double promedio(struct estructura_alumno a[n])
{
	int promedio=0;
	int i;
	
	for(i=0; i<n; i++)
	{
		promedio += *a[i].edad;
	}	
	
	return promedio/n;
}

int mayor_edad(struct estructura_alumno a[n])
{
	int mayorEdad=0;
	int i;
	
	for(i=0; i<n; i++)
	{
		if(*a[i].edad >mayorEdad) mayorEdad = *a[i].edad;
	}	
	
	return mayorEdad;
}
int menor_edad(struct estructura_alumno a[n])
{
	int menorEdad=999;
	int i;
	
	for(i=0; i<n; i++)
	{
		if(*a[i].edad <menorEdad) menorEdad = *a[i].edad;
	}	
	
	return menorEdad;
}
int nota_promedio(struct estructura_alumno a[n])
{
	int notaProm=0;
	int i;
	
	for(i=0; i<n; i++)
	{
		notaProm += *a[i].promedio;
	}	
	
	return notaProm/n;
}
int prom_mayor_igual_7(struct estructura_alumno a[n])
{
	int cantidad=0;
	int i;
	
	for(i=0; i<n; i++)
	{
		if(*a[i].promedio>=7) cantidad++;
	}	
	
	return cantidad;
}
int prom_menor_7(struct estructura_alumno a[n])
{
	int cantidad=0;
	int i;
	
	for(i=0; i<n; i++)
	{
		if(*a[i].promedio<7) cantidad++;
	}	
	
	return cantidad;
}
double porcent_mayor_igual_7(int cantidadMayor7)
{
	double cantidad= cantidadMayor7*100;
	return cantidad/n;
}
double porcent_menor_7(int cantidadMenor7)
{
	double cantidad= cantidadMenor7*100;
	return cantidad/n;
}
