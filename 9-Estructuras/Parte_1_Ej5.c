#include <stdio.h>
//Este programa imprime una estructura de tipo 'estructura_amigo' con un alias 'amigo' que guarda los valores de 'nombre', 'apellidos', 'telefono','edad', mediante un formato legible para el usuario

struct estructura_amigo {// Se crea una estructura con  una cadena 'nombre', otra 'apellidos', y una para 'telefono', ademas tambien se pone un arreglo de enteros para 'edad'
	char nombre[30]; char apellido[40]; char telefono[10]; int edad;
};

struct estructura_amigo amigo = { "Juanjo", //Se definen los valores para la estructura de tipo 'estructura_amigo' con el alias 'amigo'
	"L�pez", "983403367",
	30
};

int main() 
{
	//Se Imprimen los datos dentro del alias 'amigo' de tipo 'struct estructura_amigo' con un formato legible por el usuario
	printf( "%s tiene ", amigo.apellido ); printf( "%i a�os ", amigo.edad );
	printf( "y su tel�fono es el %s.\n" , amigo.telefono ); return 0;
}
