#include <stdio.h>
#include <conio.h>

double suma(int num[], int n);

int main() {
	
	int numeros[10], tamanio_arreglo=10;
	double promedio, suma_total=0;
	
	printf("Ingrese 10 numeros que desea sumar y obtener el promedio:\n");
	for(int i=0; i<10;i++){
		printf("(%d/10) :", i+1);
		scanf("%d",&numeros[i]);
	}
	
	//Se asigna a la varialbe suma el valor obtenido por la funcion suma(arreglo, tama�o del arreglo)
	suma_total = suma(numeros,tamanio_arreglo);
	
	// Para obtener el promedio dividimos el total de la suma de los elementos del arreglo por el tama�o del arreglo
	promedio= suma_total/tamanio_arreglo;
	
	//Se imprimen los valores obtenidos
	printf("\nLa suma de los n�meros ingresados es de: %.0f",suma_total);
	printf("\nEl promedio de los n�meros ingresados es de: %.2f",promedio);
	
	getch();
	return 0;
}

//Esta funcion realiza la suma de los elementos de un arreglo
double suma(int num[], int n){
	
	double total=0;
	
	for(int i=0; i<n; i++){
		
		total += num[i];
	}
	return total;
}
	
	
