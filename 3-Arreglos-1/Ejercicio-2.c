#include <stdio.h>
#include <conio.h>


double promedio(int num[], int tamanio);

void Imprimir_mayores(int num[], int tamanio, int promedio);


int main() {
	
	int numeros[10], tamanio_arreglo=10;
	double promediot;
	
	printf("Ingrese 10 numeros que desea obtener el promedio:\n");
	for(int i=0; i<tamanio_arreglo;i++){
		printf("(%d/10) :", i+1);
		scanf("%d",&numeros[i]);
	}
	
	// Para obtener el promedio dividimos el total de la suma de los elementos del arreglo por el tama�o del arreglo
	promediot= (double)promedio(numeros, tamanio_arreglo);
	
	//Se imprime el promedio
	printf("\nEl promedio de los n�meros ingresados es de: %.2f",promediot);
	
	//Se Imprime los n�meros mayoress al promedio
	printf("\nLos n�meros maayores al promedio son: \n");
	
	//Se usa una funcion para imprimir los numeros mayores al promedio
	Imprimir_mayores(numeros,tamanio_arreglo,promediot);
	
	getch();
	return 0;
}

//Esta funcion obtiene el promedio realiza la suma de los elementos de un arreglo y divide el resultado por el tama�o del arreglo
double promedio(int num[], int tamanio){
	
	double suma=0;
	
	for(int i=0; i<tamanio; i++){
		
		suma += num[i];
	}
	
	return suma/tamanio;
}
	
	//Esta funcion imprime  los valores mayores al promedio
	void Imprimir_mayores(int num[], int tamanio, int promedio){
		
		
		for(int i=0; i<tamanio; i++){
			
			if(num[i]>promedio) printf(" %d,",num[i]);
		}
		
	}
		
