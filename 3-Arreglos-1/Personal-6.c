#include <conio.h>
#include <stdio.h>

void imprimir_arreglo (int arreglo[], int tamanio);
void intercambiar(int* x, int* y);
void ordenar_menor_mayor(int lista[],int tamanio);
	

int main (){
	
	int arreglo1[10]={10,9,8,7,6,5,4,3,2,1}; 
	int arreglo2[10]={20,19,18,17,16,15,14,13,12,11}; 	
	int arreglo3[20];
		
	//Mostramos el arreglo 1 ordenado de menor a mayor
	printf("Arreglo 1={");
	imprimir_arreglo(arreglo1,10);
	printf("}");
	
	//Mostramos el arreglo 2 ordenado de menor a mayor
	printf("\nArreglo 2={");
	imprimir_arreglo(arreglo2,10);
	printf("}");
	
	printf("\n\nSe formara un  tercer arreglo a partir de los dos anteriores y se ordenaran sus elementos de menor a mayor: ");

	for(int i=0; i<20;i++){
		if(i<10) arreglo3[i]= arreglo1[i];
		else arreglo3[i]= arreglo2[i-10];
	}
	
	//Mostramos el arreglo 3 sin ordenar
	printf("\n\nArreglo 3 sin ordenrar={");
	imprimir_arreglo(arreglo3,20);
	printf("}");
	
	//Ordenamos los elementos de mayor a menor
	ordenar_menor_mayor(arreglo3,20);
	
	
	//Mostramos el arreglo 3 ordenado
	printf("\n\nArreglo 3 ordenado de menor a mayor={");
	imprimir_arreglo(arreglo3,20);
	printf("}");
	
	getch();
	return 0;
}
	
	//Esta Funcion imprime todos los n�meros dentro de un arreglo searados por coma y finalizando con un punto
	void imprimir_arreglo (int arreglo[], int tamanio){
		
		for(int i=0;i<(tamanio-1);i++)
		{
			printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
		}
		printf("%d.",arreglo[(tamanio-1)]); // Se imprime el ultimo valor sin coma al final y con punto (esto solo es para mayor prolijidad al mostrar los valores)
		
	}

	//Esta funcion intercambia de posicion los valores pasados
	void intercambiar(int* x, int* y)
	{
		float auxiliar;
		auxiliar = *x;
		*x = *y;
		*y = auxiliar;
	}
	
	//Esta funci�n ordena un arreglo de menor a mayor usando el algoritmo de burbuja
	void ordenar_menor_mayor(int lista[],int tamanio){
		int i,j;
		
		for(i = tamanio-1; i>0; i--){
			
			for( j=0; j < i ; j++){
				
				if (lista[j]>lista[j+1]){
					
					intercambiar(&lista[j], &lista[j+1]);
				}
			}
		}
		
	}
	
