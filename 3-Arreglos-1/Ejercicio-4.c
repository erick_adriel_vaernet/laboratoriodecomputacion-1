#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int arreglo[], int tamanio);

int main() {
	
	int numeros[10], multiplos_3[10], no_multiplos_3[10];
	int cont_1=0, cont_2=0;
	
	printf("Ingrese 10 numeros:\n");

	for(int i=0; i<10;i++){
		
		printf("(%d/10) :", i+1);
		scanf("%d",&numeros[i]);
		
		if( numeros[i] % 3 == 0)
		{	
			multiplos_3[cont_1]=numeros[i];
			cont_1++;
		}
		else 
		{
			no_multiplos_3[cont_2]=numeros[i];
			cont_2++;
		}
	}	
	
	//Se imprimen los valores obtenidos
	printf("\nEl arreglo original contiene los elementos:\n ");
	imprimir_arreglo(numeros,10);
	
	printf("\n\nEl arreglo con los multiplos de 3 contien los numeros: \n");
	imprimir_arreglo(multiplos_3,cont_1);
	
	printf("\n\nEl arreglo con los elementos que no son multiplos de 3 contien los numeros: \n");
	imprimir_arreglo(no_multiplos_3,cont_2);
	
	getch();
	return 0;
}


void imprimir_arreglo (int arreglo[], int tamanio){
	
	for(int i=0;i<(tamanio-1);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	printf("%d.",arreglo[(tamanio-1)]); // Se imprime el ultimo valor sin coma al final y con punto (esto solo es para mayor prolijidad al mostrar los valores)
	
}
	
