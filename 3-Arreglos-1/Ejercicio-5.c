#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int arreglo[], int tamanio);
void intercambiar_posiciones(int p1, int p2, int arreglo[]);


//Este programa imprime un arreglo de 10 numeros,  luego permite intecambiar de posicion sus valores 
int main() {
	
	int numeros[10]={1,2,3,4,5,6,7,8,9,10};
	int posicion1, posicion2;
	
	//Se muestra los elementos del arreglo 
	printf("\nEl arreglo contiene los siguientes numeros:\n");
	imprimir_arreglo(numeros,10);
	printf("\n0--1--2--3--4--5--6--7--8--9. (Posiciones)\n");
	
	//Se pregunta cuales quiere cambiar de posicion, luego se muestra el arreglo modificado
	do{
		
		
		printf("\nElija las posiciones a cambiar o inserte el numero 100 para salir: \n");
		scanf("%d",&posicion1);
		if(posicion1==100)return 0; //Si el usuario desea salir tiene que ingresar 100 
		scanf("%d",&posicion2);
		
		intercambiar_posiciones(posicion1,posicion2, numeros);
		
		printf("\nEl arreglo con los elementos intercambiados queda:\n");
		imprimir_arreglo(numeros,10);
		printf("\n0--1--2--3--4--5--6--7--8--9. (Posiciones)\n\n");
		
	}while(posicion2 !=100);
	return 0;
}

//Esta Funcion imprime todos los n�meros dentro de un arreglo searados por coma y finalizando con un punto
void imprimir_arreglo (int arreglo[], int tamanio){
	
	for(int i=0;i<(tamanio-1);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	printf("%d.",arreglo[(tamanio-1)]); // Se imprime el ultimo valor sin coma al final y con punto (esto solo es para mayor prolijidad al mostrar los valores)
	
}
	
	
	//Esta Funcion intercambia de posicion 2 valores dentro de un arreglo
	void intercambiar_posiciones(int p1, int p2, int arreglo[]){
		int valor_copia=arreglo[p1];
		arreglo[p1]=arreglo[p2];
		arreglo[p2]=valor_copia;
		
	}
		
