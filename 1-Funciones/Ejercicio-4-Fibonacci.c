#include <stdio.h>
#include <conio.h>

void fibonacci(int n);

int main() {
	printf("Los 15 primeros n�meros de la sucesi�n de Fibonacci son: \n");
	fibonacci(15);
	getch();
	return 0;
}

void fibonacci(int n){
	int i, x=0, y=1, z;
	for(i=1;i<n;i++){
		printf("%d, ",x);
		z= x+y;
		x=y;
		y=z;
	}
	printf("%d.",x);
}
