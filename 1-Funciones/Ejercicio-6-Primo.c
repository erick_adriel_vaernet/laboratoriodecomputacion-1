#include <stdio.h>
#include <conio.h>

int PRIMO(int n){      //Esta funci�n retorna si 1 si el numero es primo y 0 si no lo es
	int divisor = 2, primo = 1;
	
	while ((divisor < n) && (primo != 0)){ //Se dvide el numero "n" dado por todos los numeros posibles que sean mayores a 1 y menores a "n"
		if (n%divisor == 0) primo = 0; //si encuentra un divisor exacto dentro del rrango 1-"n" el numero no es primo por lo que se retorna 0 
		divisor++;
	}
	return primo; //si el numero n encontro un divisor exacto en el rango dado quiere decir que solo es divisible or 1 y por si mismo (es primo y se retorna 1)
}
	
int main(){ // Este programa permite ingresar diferentes valores, se comprobara cuales son primos y se los contara hasta que el usuario ingrese 0;
	
	int num, esprimo = 0, num_primos=0;
	printf("Este programa permite ingresar diferentes valores, se comprobara cuales son primos y se los contara hasta que el usuario ingrese 0\n");
	for (int i = 1; i > 0; i++){
		
		printf("Ingrese un numero: ");
		scanf("%d", &num);
		if (num == 0) break;
		esprimo = PRIMO(num);
		if (esprimo == 1) num_primos++;
		
	}
	printf("La cantidad de numeros primos ingresados es de: %d", num_primos);
	getch();
	return 0;
}
	
