#include<stdio.h>
#include <conio.h>

void ingresar_valores (int arreglo[], int tamanio);
int multiplicar(int arreglo[], int tamanio);
void mostrar_ingresados(int arreglo[], int tamanio);

int main()
{
	int n, multiplicacion;
	printf("Introduzca la cantidad de numeros a ingresar: ");
	scanf("%d",&n);
	int numeros[n];
	ingresar_valores(numeros , n);
	multiplicacion = multiplicar(numeros, n);//Se asigana a total el valor obtenido del producto de los numeros ingresados (que es retorando por la funcion correspondiente) 
	printf("\nLos n�meros ingresados son: ");
	mostrar_ingresados(numeros, n);; //Esta funcion imprime los numeros ingresados separados por coma ","
	printf("\nEl producto de los n�meros ingresados es: %d",multiplicacion);
	getch();
	return 0;
}

void ingresar_valores(int arreglo[],int tamanio) //Permite ingresar n valores y retornar su producto
{
	int i;
	for(i=0;i<tamanio;i++)
	{
		printf("Ingrese un n�memro entero (%d/%d):\n",(i+1),tamanio);
		scanf("%d",&arreglo[i]); //En cada ciclo se guarda el valor ingresado
	}
}

int multiplicar(int arreglo[], int tamanio){
	int i, producto=1;
	for(i=0;i<tamanio;i++)
	{
		producto*= arreglo[i]; //Se realiza el producto de los valores del arreglo, pasado por parametro, en cada iteracion
	}
	return producto;
}

void mostrar_ingresados(int arreglo[], int tamanio) //Funcion que imprime los numeros ingresados
{
	int i;
	for(i=0;i<(tamanio-1);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	printf("%d.",arreglo[i]); // Se imprime el ultimo valor sin coma al final y con punto (esto solo es para mayor prolijidad al mostrar los valores)
}
