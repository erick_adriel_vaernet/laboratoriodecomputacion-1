#include <stdio.h>
#include <conio.h>

int factorial(int n);

int main(int argc, char *argv[]) {
	int num;
	printf("Ingresa un n�mero natural para calcular su factorial: ");
	scanf("%d",&num);
	printf("\nEl factorial de %d es: %d", num, factorial(num));
	getch();
	return 0;
}



int factorial(int n){ //Funcion que retorna el factorial de un n�mero dado por parametro;
	int i,nfactorial=1;
	for(i=1;i<=n;i++){
		nfactorial=i*nfactorial;
	}
	return nfactorial;
}

