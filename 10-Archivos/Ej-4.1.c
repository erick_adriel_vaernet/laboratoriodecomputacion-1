#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

//4.3: Dise�ar un programa que permita generar un archivo de texto que permita cargar cinco nombres separados por punto. Mostrar el contenido del archivo un nombre abajo del otro.

FILE * archivo;
char nombres [100];

int escribir_archivo();
void leer_archivo(int longitud);

int main(int argc, char *argv[]) {
	
	int longit;
	longit = escribir_archivo();
	
	leer_archivo(longit);

	getch();
	return 0;
}

int escribir_archivo(){
	int longitud=0,cant;
	
	archivo = fopen ("nombres.txt","w");
	
	if(archivo==NULL)printf("Error al abrir el archivo para escritura");
	else{
		printf("Ingrese 5 nombres separados por puntos (Ejemplo:erick.ian.adriel.alex.juan):\n");
		gets(nombres);
		
		longitud = strlen(nombres);
		
		cant = fwrite(nombres, sizeof(char), longitud, archivo);
		
		if(cant<longitud)printf("Error en la escritura");
	}
	
	fclose(archivo);
	
	return longitud;
}

void leer_archivo(int longitud){
	
	
	archivo= fopen("nombres.txt","r");
	
	printf("\nNombres en el archivo:\n");
	
	if(longitud != fread(nombres,sizeof(char),longitud, archivo)) printf("Error al leer el archivo");
	else {
		for(int i=0;i<longitud;i++){
			if(nombres[i]!='.') printf("%c",nombres[i]);
			else printf("\n");			
		};
	}
	
	
}

