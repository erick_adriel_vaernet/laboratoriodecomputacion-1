#include<stdio.h>
#include<conio.h>
#include<string.h>

FILE *arch;

struct registro
{
	int dni;
	char apellido[20];
	char nombre[20];
	float nota;
};

void leer_archivo();

int main()
{	
	struct registro reg;
	char seguir, leer;

	
	if ((arch=fopen("Ej5.dat","wb"))==NULL)	printf("No se pudo abrir el archivo para escritura");
	
	do
	{
		printf("\nIngrese n�mero de DNI: ");
		scanf("%d",&reg.dni);
		
		printf("\nIngrese el nombre del alumo: ");
		scanf("%s",&reg.nombre);
		
		printf("\nIngrese el apellido del alumno: ");
		scanf("%s",&reg.apellido);
		
		printf("\nIngrese la nota del alumno: ");
		scanf("%f",&reg.nota);
		
		fwrite(&reg,sizeof(reg),1,arch);
		
		printf("desea terminar s/n: ");
		scanf("\n%c",&seguir);
		
		
	}
	while(seguir=='n');
	
	fclose(arch);
	
	printf("�desea Leer los datos del archivo? s/n :");
	
	scanf("\n%c",&leer);
	
	if(leer=='s'){
		leer_archivo();
	}	
	
	getch();
	return 0;
}


void leer_archivo(){
	struct registro reg_lectura;
	struct registro alumno_mayor_nota;
	struct registro alumno_menor_nota;
	
	int cant_lectura,num_alumnos=0, alumnos_aprobados=0, alumnos_desaprobados=0;
	
	float nota_mayor=0, nota_menor=99,
		porcentaje_ap, porcentaje_des;
	
	double promedio,notas=0;
	
	
	if ((arch=fopen("Ej5.dat","rb"))==NULL)	printf("No se pudo abrir el archivo para lectura");
	
	
	printf("\nDni\t\tNombre\t\tApellido\t\t\tNota");
	printf("\n----------------------------------------------------------------------------");
	while(!feof(arch)){
		
		cant_lectura=fread(&reg_lectura,sizeof(reg_lectura),1,arch);
		
		if(cant_lectura!=1){
			if(feof(arch)){
				break;
			}
			else{
				printf("\nError en la lectura del archivo");
				break;
			}
		}
		
		if (reg_lectura.nota >= nota_mayor){
			nota_mayor= reg_lectura.nota;
			
			alumno_mayor_nota.dni= reg_lectura.dni;
			strcpy( alumno_mayor_nota.nombre, reg_lectura.nombre);
			strcpy( alumno_mayor_nota.apellido, reg_lectura.apellido);
			alumno_mayor_nota.nota= reg_lectura.nota;
		}
		
		if (reg_lectura.nota <= nota_menor){
			nota_menor= reg_lectura.nota;
			
			alumno_menor_nota.dni= reg_lectura.dni;
			strcpy (alumno_menor_nota.nombre, reg_lectura.nombre);
			strcpy (alumno_menor_nota.apellido, reg_lectura.apellido);
			alumno_menor_nota.nota= reg_lectura.nota;
		}
		
		if(reg_lectura.nota>=6) alumnos_aprobados++;
		else alumnos_desaprobados++;
		
		notas+=reg_lectura.nota;
		num_alumnos++;
		
		printf("\n%i\t\t%s\t\t%s\t\t\t%.2f",reg_lectura.dni,reg_lectura.nombre,reg_lectura.apellido,reg_lectura.nota);
		
	}
	
	printf("\n----------------------------------------------------------------------------");
	printf("\n\nAlumno con la nota m�s alta:");
	printf("\nDNI: %i; Nombre: %s; Apellido: %s; Nota%.2f",alumno_mayor_nota.dni,alumno_mayor_nota.nombre,alumno_mayor_nota.apellido,alumno_mayor_nota.nota);
	
	printf("\n\nAlumno con la nota m�s baja:");
	printf("\nDNI: %i; Nombre: %s; Apellido: %s; Nota%.2f",alumno_menor_nota.dni,alumno_menor_nota.nombre,alumno_menor_nota.apellido,alumno_menor_nota.nota);
	
	promedio=notas/num_alumnos;
	printf("\n\nPromedio general: %.2f",promedio);
	
	printf("\n\nCantidad de alumnos con nota mayor e igual a 6: %i",alumnos_aprobados);
	
	printf("\n\nCantidad de alumnos con nota menor a 6: %i",alumnos_desaprobados);
	
	porcentaje_ap= (alumnos_aprobados*100)/num_alumnos;	
	printf("\n\nPorcentaje de alumnos con nota mayor e igual a 6 : %.2f", porcentaje_ap);
	
	
	porcentaje_des= (alumnos_desaprobados*100)/num_alumnos;
	printf("\n\nPromedio de alumnos con nota menor a 6 : %.2f",porcentaje_des);
	
	
	fclose(arch);
	
	
}
