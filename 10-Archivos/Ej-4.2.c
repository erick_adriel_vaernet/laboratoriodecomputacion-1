#include<stdio.h>
#include<conio.h>
#include<string.h>
#define a "arch.dat" //Se cambio la direccion por el hecho de que en el directorio C se requieren permisos.

FILE *arch;

	struct registro
{
	int cliente;
	char nombre[20];
	char apellido[20];
	float saldo;
	
};

void leer_archivo(int tam_reg);

int main()
{	
	struct registro reg;
	char seguir, leer;
	int tamm_reg;
	int cantidad_reg=0;
	
	if ((arch=fopen(a,"wb"))==NULL)	printf("No se pudo abrir el archivo para escritura ");
	
	do
	{
		printf("\nIngrese numero de cliente: ");
		scanf("%d",&reg.cliente);
		
		printf("\nIngrese el nombre: ");
		scanf("%s",&reg.nombre);
		
		printf("\nIngrese el apellido: ");
		scanf("%s",&reg.apellido);
		
		printf("\nIngrese el saldo: ");
		scanf("%f",&reg.saldo);
		
		fwrite(&reg,sizeof(reg),1,arch);
		cantidad_reg++;
		
		printf("desea terminar s/n: ");
		scanf("\n%c",&seguir);
		
		if(seguir=='n') tamm_reg=sizeof(reg); //se obtiene el tama�o en bytes del registro
	}
	while(seguir=='n');
	
	fclose(arch);
	
	printf("�desea Leer los datos del archivo? s/n :");
	
	scanf("\n%c",&leer);
	
	if(leer=='s'){
		leer_archivo(tamm_reg);
	}

	
	
	getch();
	return 0;
}

//funci�n que muestra el contenido del archivo
void leer_archivo(int tam_reg){
	struct registro reg_lectura;
	
	int cant_lectura,
		total_socios=0;
	
	float total_saldos=0;
	
	
	if ((arch=fopen(a,"rb"))==NULL)	printf("No se pudo abrir el archivo para lectura");
	
	
	printf("\nCliente\t\tNombre\t\tApellido\t\t\tSaldo");
	printf("\n----------------------------------------------------------------------------");
	while(!feof(arch)){
		
		cant_lectura=fread(&reg_lectura,sizeof(reg_lectura),1,arch);
		
		if(cant_lectura!=1){
			if(feof(arch)){
				break;
			}
			else{
				printf("error en lectura de archivo");
				break;
			}
		}
		
		
		total_socios++;
		total_saldos+=reg_lectura.saldo;
		
		printf("\n%i\t\t%s\t\t%s\t\t\t%.2f",reg_lectura.cliente,reg_lectura.nombre,reg_lectura.apellido,reg_lectura.saldo);
		
	}
	
	printf("\n----------------------------------------------------------------------------");
	printf("\n\nTotal Socios: %i",total_socios);
	printf("\nTotal Saldos: %.2f", total_saldos);
	
	fclose(arch);
	
	
}

