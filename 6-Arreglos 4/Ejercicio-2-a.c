#include <stdio.h>
#include <conio.h>
	
void imprimir_arreglo (int array[], int size);
double promedio_arreglo(int array[],int size);

//Este programa permite al usuario ingresar 10 n�meros que formaran parte de un arreglo, se mostrara el promedio y la cantidad de n�meros mayores a el
int main() {
	
	int arreglo [10];
	int n,i,j;
	int flag;
	double promedio;
	
	//Bucle para que el usuario ingrese los elementos del arreglo
	for(i=0; i<10; i++)
	{
		printf("Ingrese el elemento %d del arreglo:\n ",i);
		scanf("%d",&n);
		arreglo[i]=n;
	}
	
	//Se imprime los elementos del arreglo 
	printf("\nArreglo con los elementos ingresados:\n");
	printf("Arreglo");
	imprimir_arreglo(arreglo,10);
	
	//Se obtiene el promedio de un arreglo(mediante la siguiente funcion)
	promedio = promedio_arreglo(arreglo,10);
	
	//recorremos el arreglo, si el valor en la posici�n j es mayor que el promedio se vmuestra en pantalla
	for(j=0; j<10;j++)
	{
		if(arreglo[j]>promedio && flag==0)
		{
			printf("\n N�meros mayores al promedio (%.2f) : ",promedio);
			flag=1;
		}
		if(arreglo[j]>promedio) printf("%d, ",arreglo[j]);
	}
	
	
	getch();
	return 0;
}

//Este procedimiento imprime los elementos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int array[], int size){
	
	printf(" = { "); 
	
	for(int i=0;i<(size-1);i++)
	{
		printf("%d, ",array[i]); // En cada iteraci�n se imprime los diferentes valores dentro del arreglo separados por coma, excepto el �ltimo valor
	}
	
	printf("%d }",array[(size-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
//Esta funci�n devuelve el promedio de un arreglo de enteros; sus parametros son el arreglo y el tama�o del mismo;
double promedio_arreglo(int array[],int size){
	int i;
	double acumulador, promedio;
	for(i=0; i<size; i++)
	{
		acumulador= acumulador +  array[i];
	}
	promedio = acumulador / size;
	return promedio;
}
	
