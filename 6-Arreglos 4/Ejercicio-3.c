#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int array[], int size);
void insert_sort(int array[], int size);

/*Este programa crea 2 arreglos a partir deuno ya definido previamente, en uno se ingresan los numeros multiplos de 3 y en el otro el resto de los numeros; muestra los 3 arreglos y luego
se ordena los elementos del arreglo con multiplos de 3 de menor a mayor usando un m�todo de ordenamiento diferente al de burbujay por ultimo se Completa el arreglo con m�ltiplos de tres manteniendose ordenado.
Ademas de indicar cuantos elementos fueron agregados.*/
int main() {
	
	int arreglo [10]={3,2,1,4,10,5,7,9,6,8};
	int multiplosDe3 [10];
	int noMultiplos [10];
	int i,j,m=0,n=0;
	int agregados=0;
	
	//Se agregan eleementos a 2 arreglos, a uno con los multiplos de 3 del arreglo original y otro con elr esto de los numeros
	for(i=0; i<10; i++)
	{
		if(arreglo[i]%3 == 0)
		{
			multiplosDe3[m] = arreglo[i];
			m++;
		}
		else
		{
			noMultiplos[n] = arreglo[i];
			n++;
		}
	}
	
	//Se imprime los elementos del arreglo original
	printf("\nArreglo original:\n");
	printf("Arreglo");
	imprimir_arreglo(arreglo,10);
	
	//Se imprime los elementos del arreglo con multiplos de 3
	printf("\n\nArreglo con n�meros multiplos de 3:\n");
	printf("multiplosDe3");
	imprimir_arreglo(multiplosDe3,m);
	
	//Se imprime los elementos del arreglo con los numeros no multiplos de 3
	printf("\n\nArreglo con n�meros que no son multiplos de 3:\n");
	printf("noMultiplos");
	imprimir_arreglo(noMultiplos,n);
	
	//Se ordena de menor a mayor los elementos del arreglo con multiplos de 3 mediante el metodo de inserci�n
	insert_sort(multiplosDe3, m);
	
	//Se imprime los elementos del arreglo con multiplos de 3 ordenados de menor a mayor
	printf("\n\nArreglo con n�meros multiplos de 3 ordenados mediante el metodo de inserci�n:\n");
	printf("multiplosDe3");
	imprimir_arreglo(multiplosDe3,m);
	
	//Se agregan otros multiplos de 3 mayores al ultimo elemento del arreglo
	//Esto se hace mediante un bucle en el cual por cada iteraci�n se agrega un nuevo elemento igual a la suma de 3  mas el elemento anterior
	for(j=m; j<10; j++)
	{
		multiplosDe3[j] =multiplosDe3[j-1] + 3;
		agregados++;
	}
	
	//Se imprime los elementos del arreglo con multiplos de 3 ordenados de menor a mayor y completado con otros multiplosde 3
	printf("\n\nArreglo con n�meros multiplos de 3 ordenados mediante el metodo de inserci�n y completado con otros multiplos de 3:\n");
	printf("multiplosDe3");
	imprimir_arreglo(multiplosDe3,10);
	
	//Se muestran la cantidad de n�merosmultiplos de 3 agregados
	printf("\nMultiplos de 3 agregados al arreglo: %d", agregados);
		
	getch();
	return 0;
}

//Este procedimiento imprime los elementos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int array[], int size){
	
	printf(" = { "); 
	
	
	for(int i=0;i<(size-1);i++)
	{
		printf("%d, ",array[i]); // En cada iteraci�n se imprime los diferentes valores dentro del arreglo separados por coma, excepto el �ltimo valor
	}
	
	printf("%d }",array[(size-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}

void insert_sort(int array[], int size)
{
	int i, j, temp;
	for(i=0; i<size; i++)
	{
		temp = array[i];
		j = i-1;
		while(j>=0 && array[j] >temp)
		{
			array[j+1] = array[j];
			j--;
		}
		
		array[j+1] = temp;
	}
}
	
