#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int array[], int size);
int ordenar_burbuja(int array[], int size);
int eliminar_repetidos(int array[], int size);
int insertar_numeros(int array[],int size, int ultPosic);
int sumar_elementos(int array[],int size);
double promedioo(double suma,int cantElem);

//Este programa permite al usuario ingresar 10 numeros que formaran parte de un arreglo, se mostrara el arreglo desordenado, luego se ordenara este arreglo mediante el m�todo Burbuja,se vuelve a mostrar el arreglo ordenado
//Se eliminan elementos repetidos y se insertan nuevos elementos par finalmente calcular la suma y el promedio de los elementos dentro del arreglo
int main(int argc, char *argv[]) {
	
	int arreglo [10];
	int i,n,todoOk, eRepetidos, elemInsertados;
	double suma;
	double promedio;
	
	//Bucle para que el usuario ingrese los elementos del arreglo, desordenado
	for(i=0; i<10; i++)
	{
		printf("Ingrese el elemento %d del arreglo:\n ",i);
		scanf("%d",&n);
		arreglo[i]=n;
	}
	
	//Se imprime los elementos del arreglo desordenado
	printf("\nArreglo desordenado con los elementos ingresados:\n");
	printf("Arreglo");
	imprimir_arreglo(arreglo,10);
	
	//Se ordena el arreglo mediante el m�todo de la burbuja y se guarda el valor de retorno (1 en caso de que se haya ordenado sin errores) en la variable todoOK
	todoOk = ordenar_burbuja(arreglo,10);
	
	//if auxiliar, en caso de que la funci�n de ordenamiento retorne un numero diferente a 1,quiere decir que hubo un error; se avisa al usuario
	if(todoOk != 1){
		printf("\n**Hubo un error en el ordenamiento**\nn");
	}
	
	//Se muestra el arreglo ordenado segun el metodo burbuja
	printf("\n\nArreglo ordenado de mayor a menor con el metodo burbuja:\n");
	printf("Arreglo");
	imprimir_arreglo(arreglo,10);

	//se eliminan elementos repetidos y se guarda loa cantidad de elementos eliminados en una variable
	eRepetidos = eliminar_repetidos(arreglo, 10);
	
	//Se muestra el arreglo ordenado segun el metodo burbuja
	printf("\n\nArreglo Ordenado sin elementos repetidos:\n");
	printf("Arreglo");
	imprimir_arreglo(arreglo,10-eRepetidos);
	printf("\nCantidad de elementos repetidos eliminados= %d\n",eRepetidos);
	
	//Se insertan numeros en el arreglo donde se eliminaron los elementos repetidos hasta completarlo; se devuelve la cantidad de elementos insertados
	elemInsertados = insertar_numeros(arreglo,10, 9-eRepetidos);
	
	//Se muestra el arreglo con los nuevos elementos agregados
	printf("\n\nArreglo con los nuevos elementos insertados:\n");
	printf("Arreglo");
	imprimir_arreglo(arreglo,10);
	printf("\nCantidadlementos nuevos insertados: %d\n",elemInsertados);
	
	//Se suman los elementos del arreglo y se obtiene el promedio de los mismos
	suma = sumar_elementos(arreglo,10);
	promedio= promedioo(suma,10);
	
	//Se muestra el promedio
	printf("\nEl promedio de los elementos del arregloe s de: %.2f", promedio);
	
	getch();
	return 0;
}

//Este procedimiento imprime los elementos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int array[], int size){
	
	printf(" = { "); 
	
	for(int i=0;i<(size-1);i++)
	{
		printf("%d, ",array[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	
	printf("%d }",array[(size-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
	//Esta funci�n ordena un arreglo mediante el m�todo de la burbuja y retorna 1 si finaliza sin errores
int ordenar_burbuja(int array[], int size){
	int i,j, aux;
	for(i=1; i<size; i++)
	{
		for(j=0; j<size-i; j++)
		{
			if(array[j]<array[j+1])
			{
				aux    = array[j+1];
				array[j+1] = array[j];
				array[j]   = aux;
			}
		}
	}
	return 1;	
}
	
//Esta funci�n elimina los numeros repetidos y devuelve la cantidad de elementos eliminados 
int eliminar_repetidos(int array[], int size)
{
	int i=1,j;
	int repetidos=0;
	int limite = size-1;

	//mientras i sea menor a la posici�n limite (esta misma var�a al eliminar los elementos repetidos)
	while(i<=limite){
		
		if(array[i] == array[i-1])
		{
			repetidos++;
			
			for(j=i-1;j<limite; j++)
			{
				array[j] = array[j+1];
				if(j==limite-1) array[j+1]='\0';
			}
			
			limite--;
			
			if(array[i]==array[i-1]) continue;
		}
		i++;
	}
	return repetidos;
}

//funci�n que inserta un numero determinado de elementos en un arreglo
int insertar_numeros(int array[],int size,int ultPosic)
{
	int i, agregados=0;
	for(i=ultPosic+1; i<size; i++)
	{
		array[i]= array[i-1]-1;
		agregados++;
	}
	return agregados;		
}

//funci�n que suma elementos de un arreglo y retorna el total
int sumar_elementos(int array[],int size)
{
	int i, suma=0;
	for(i=0; i<size; i++)
	{
		suma+=array[i];
	}
	printf("\n La suma de los elementos del arreglo es de: %d",suma);
	return suma;
}
double promedioo(double suma,int cantElem)
{
	return suma/cantElem;
}
