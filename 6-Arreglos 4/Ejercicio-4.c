#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int array[], int size);
void insert_sort(int array[], int size);
int busqueda_binaria (int num_buscado, int arreglo [], int tamanio);

/* Dados dos arreglos ordenado de mayor a menor sin elementos repetidos de 10 elementos cada uno.este programa:
i.	Genera un nuevo vector ordenado que contiene a ambos vectores. Se muestra el resultado
ii.	Muestra el vector de menor a mayor
iii.	permite ingresar un elemento por teclado y verificar que se encuentra en el arreglo usando b�squeda binaria
*/
int main() {
	
	int arreglo_1 [10]={10,9,8,7,6,5,4,3,2,1};
	int arreglo_2 [10]={20,19,18,17,16,15,14,13,12,11};
	int arreglo_3 [20];
	int i;
	int buscado,posicElemBuscado;
	
	//Se generan los elementos del nuevo vector (arreglo_3) a partir de los 2 arreglos ya definidos
	for(i=0; i<20; i++)
	{
		if(i<10) arreglo_3[i] =arreglo_1[i];
		else arreglo_3[i]=arreglo_2[i-10];
	}
	
	//Se imprime en pantalla los elementos del arreglo_1
	printf("\nPrimer arreglo ordenado de mayor a menor:\n");
	printf("arreglo_1");
	imprimir_arreglo(arreglo_1,10);
	
	//Se imprime en pantallalos elementos del arreglo_2
	printf("\n\nSegundo arreglo ordenado de mayor a menor:\n");
	printf("arreglo_2");
	imprimir_arreglo(arreglo_2,10);
	
	//Se imprime en pantallalos elementos del arreglo_3 formado por la union de los 2 anteriores
	printf("\n\nTercer arreglo formado por la union de los dos anteriores:\n");
	printf("arreglo_3");
	imprimir_arreglo(arreglo_3,20);
	
	//Se ordena de menor a mayor los elementos del arreglo_3
	insert_sort(arreglo_3,20);
	
	
	//Se imprime en pantallalos elementos del arreglo_3 ordenado de menor a mayor
	printf("\n\nTercer arreglo ordenado de menor a mayor mediante el metodo de inserci�n;\n");
	printf("arreglo_3");
	imprimir_arreglo(arreglo_3,20);
	
	//se pide ingresar un numero a buscar dentro del arreglo 3
	printf("\n\nIngrese un numero para comprobar si esta en el arreglo 3 mediante busqueda binaria:\n");
	scanf("%d",&buscado);
	
	//Se busca el elemento ingresado en el arreglo mediante una funcion que devuelve la posicion del mismo 
	posicElemBuscado = busqueda_binaria(buscado, arreglo_3, 20);
	
	//Se comprueba que el elemento buscado estee en el arreglo, de no ser as� se emite un mensaje de que no esta en el arreglo
	if(posicElemBuscado == -1) printf("\n El elemento buscado no se encuentra en el arreglo_3");
	//Si se encuentra,se imprime la posicion del numero buscado
	else printf("\nEl numero %d se encuentra en la posicion %d del arreglo_3", buscado, posicElemBuscado);
	
	
	getch();
	return 0;
}

//Este procedimiento imprime los elementos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int array[], int size){
	
	printf(" = { "); 
	
	
	for(int i=0;i<(size-1);i++)
	{
		printf("%d, ",array[i]); // En cada iteraci�n se imprime los diferentes valores dentro del arreglo separados por coma, excepto el �ltimo valor
	}
	
	printf("%d }",array[(size-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
void insert_sort(int array[], int size)
{
	int i, j, temp;
	for(i=0; i<size; i++)
	{
		temp = array[i];
		j = i-1;
		while(j>=0 && array[j] >temp)
		{
			array[j+1] = array[j];
			j--;
		}
		
		array[j+1] = temp;
	}
}


//Esta funci�n permite buscar un entero dentro de un arreglo con busqueda binaria y devolver la posicion donde se encuentra, en caso de no encontrarlo retornara -1
int busqueda_binaria ( int num_buscado, int arreglo [], int tamanio) {
	int izq, der, centro;
	izq = 0;
	der = tamanio - 1;
	centro = (izq+der)/2;
	while ((arreglo[centro] != num_buscado) && (izq < der)) {
		if (arreglo [centro] < num_buscado) {
			izq = centro + 1 ;
		}
		else {
			der = centro - 1;
		}
		centro = (izq + der) / 2;
	}
	if (num_buscado == arreglo [centro]) {
		return (centro);
	}
	else {
		return (- 1);
	}
}

	
