#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int array[], int size);
double promedio_arreglo(int array[],int size);
void mostrar_mayores_a(double numero, int array[], int size);

//Este programa permite al usuario ingresar 10 n�meros que formaran parte de un arreglo, se mostrara el promedio y la cantidad de n�meros mayores a el
int main() {
	
	int arreglo [10];
	int n,i;
	double promedio;
	
	//Bucle para que el usuario ingrese los elementos del arreglo
	for(i=0; i<10; i++)
	{
		printf("Ingrese el elemento %d del arreglo:\n ",i);
		scanf("%d",&n);
		arreglo[i]=n;
	}
	
	//Se imprime los elementos del arreglo 
	printf("\nArreglo con los elementos ingresados:\n");
	printf("Arreglo");
	imprimir_arreglo(arreglo,10);
	
	//Se obtiene el promedio de un arreglo(mediante la siguiente funcion)
	promedio = promedio_arreglo(arreglo,10);
	
	//se muestra mediante una funcion los elementos mayores al promedio
	
	mostrar_mayores_a(promedio,arreglo, 10);
	

	getch();
	return 0;
}

//Este procedimiento imprime los elementos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int array[], int size){
	
	printf(" = { "); 
	
	for(int i=0;i<(size-1);i++)
	{
		printf("%d, ",array[i]); // En cada iteraci�n se imprime los diferentes valores dentro del arreglo separados por coma, excepto el �ltimo valor
	}
	
	printf("%d }",array[(size-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
//Esta funci�n devuelve el promedio de un arreglo de enteros; sus parametros son el arreglo y el tama�o del mismo;
double promedio_arreglo(int array[],int size){
	int i;
	double acumulador, promedio;
	for(i=0; i<size; i++)
	{
		acumulador= acumulador +  array[i];
	}
	promedio = acumulador / size;
	return promedio;
}

//Esta funci�n recorre el arreglo si el valor j es mayor al numero indicado se imprime el valor
void mostrar_mayores_a(double numero, int array[], int size)
{
	int j,flag;
	
	for(j=0; j<size;j++)
	{
		if(array[j]>numero && flag==0)
		{
			printf("\n N�meros mayores al promedio (%.2f) : ",numero);
			flag=1;
		}
		if(array[j]>numero) printf("%d, ",array[j]);
	}
}
