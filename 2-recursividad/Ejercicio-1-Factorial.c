#include <stdio.h>
#include <conio.h>

// Programa que realiza los 10 primeros numeros de la serie de fibonacci, con funcion recursiva.

int fibo (int n);

int main (){
	
	printf("Serie Fibonacci:\n\n");
	
	for (int i = 0; i <= 9; i++){
		
		printf("%d, ",fibo(i)); //mostramos la serie llamando a la funcion recursiva.
	}
	
	getch();
}
	
int fibo (int n){
	
	if (n < 2) return n; // caso base
	else return (fibo(n-2)+ fibo(n-1)); // caso recursivo
	
}
