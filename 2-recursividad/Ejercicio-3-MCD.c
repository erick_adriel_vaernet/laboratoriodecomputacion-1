#include <stdio.h>
#include <conio.h>

//programa que realiza el triangulo de Pascal

int triangpascal(int fila, int col){
	
	if (col == 0 || fila == col) return 1; //caso base 
	else return (triangpascal((fila - 1),(col - 1)) + triangpascal((fila-1), col)); //caso recursivo
}
	
int main (){
	
	int grado;
	
	printf(" Ingrese el grado del binomio o numero de fila: "); // se solicita las filas o grado de binomio
	scanf("%d", &grado);
	
	for (int i = 0; i <= grado; i++){
		
		for (int j = 0; j <= i; j++){
			printf (" %d",triangpascal(i, j)); // se muestra cada el elementos de los binomios.
		}
		
		printf("\n");
	}
	
	getch();
}
