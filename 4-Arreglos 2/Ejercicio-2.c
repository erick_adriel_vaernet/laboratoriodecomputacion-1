#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int arreglo[], int tamanio);
int busqueda_binaria (int arreglo [], int tamanio, int num_buscado);

//Este programa imprime un arreglo A en pantalla, el usuario puede elegir un numero a buscar dentro del arreglo
//el programa buscara el numero con busqueda binaria y en caso de que est�, le dira en que posicion se encuentra,

int main() {
	int A[8] = { 1,4,6,8,10,11,15,16 };
	int n_buscado, posicion=-1;
	
	printf("A");
	imprimir_arreglo(A,8);
	
	printf("\nIngrese el numero que desea buscar en el arreglo:\n");
	scanf("%d",&n_buscado);
	
	posicion = busqueda_binaria(A,8,n_buscado);
	
	if(posicion == -1) printf("El numero que esta buscando no se encuentra en el arreglo A");
	else printf("El numero %d se encuentra en la posicion %d del arreglo A", n_buscado, posicion);
	
	getch();
	return 0;
}


//Este procedimiento imprime los elememntos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int arreglo[], int tamanio){
	
	printf(" = { "); 
	
	for(int i=0;i<(tamanio-1);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	
	printf("%d }",arreglo[(tamanio-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
//Esta funci�n permite buscar un entero dentro de un arreglo con busqueda binaria y devolver la posicion donde se encuentra, en caso de no encontrarlo retornara -1
int busqueda_binaria (int arreglo [], int tamanio, int num_buscado) {
	int izq, der, centro;
	izq = 0;
	der = tamanio - 1;
	centro = (izq+der)/2;
	while ((arreglo[centro] != num_buscado) && (izq < der)) {
		if (arreglo [centro] < num_buscado) {
			izq = centro + 1 ;
		}
		else {
			der = centro - 1;
		}
		centro = (izq + der) / 2;
	}
	if (num_buscado == arreglo [centro]) {
		return (centro);
	}
	else {
		return (- 1);
	}
}


