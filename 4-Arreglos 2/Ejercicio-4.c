#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int arreglo[], int tamanio);
int buscar_arreglo (int num_buscado,int arreglo[], int tamanio);

//Este programa imprime un arreglo A en pantalla, el usuario puede elegir un numero a buscar dentro del arreglo
//el programa buscara el numero y en caso de que est�, le dira en que posicion se encuentra,
int main() {
	int A[8] = { 5,4,10,8,2,11,9,1 };
	int n_buscado, posicion;
	
	printf("A");
	imprimir_arreglo(A,8);
	
	printf("\nIngrese el numero que desea buscar en el arreglo:\n");
	scanf("%d",&n_buscado);
	
	posicion = (buscar_arreglo(n_buscado,A,8));
	
	if(posicion == -1) printf("El numero que esta buscando no se encuentra en el arreglo A");
	else printf("El numero %d se encuentra en la posicion %d del arreglo A", n_buscado, posicion);
	
	getch();
	return 0;
}

//Este procedimiento imprime los elememntos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int arreglo[], int tamanio){
	
	printf(" = { "); 
	
	for(int i=0;i<(tamanio-1);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	
	printf("%d }",arreglo[(tamanio-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
//Esta funci�n permite buscar un entero dentro de un arreglo  y devolver la posicion donde se encuentra, en caso de no encontrarlo retornara -1
int buscar_arreglo (int num_buscado,int arreglo[], int tamanio){
	int i=0,encontrado=0;
	
	while(!encontrado && i<tamanio){
		
		if(arreglo[i]==num_buscado){
			
			encontrado=1;
			
		}else{
			
			i++;
		}
	}
	if(encontrado){
		
		return i;
		
	}else{ 
		
		return -1;
		
	}
}
	
	
