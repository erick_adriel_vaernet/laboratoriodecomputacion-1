#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int arreglo[], int tamanio);
void busqueda_binaria (int num_buscado,int arreglo[], int tamanio, int posiciones[]);


//Este programa imprime un arreglo A en pantalla, el usuario puede elegir un numero a buscar dentro del arreglo
//el programa buscara el numero y en caso de que est�, le dira cuantas veces se repite y en que posicion se encuentra,
int main() {
	int A[11] = { 1,4,6,6,8,10,11,11,15,16,16 };
	int posicioness[11];
	int n_buscado;
	
	printf("A");
	imprimir_arreglo(A,11);
	
	printf("\nIngrese el numero que desea buscar en el arreglo:\n");
	scanf("%d",&n_buscado);
	
	busqueda_binaria(n_buscado,A,11, posicioness);
	
	getch();
	return 0;
}

//Este procedimiento imprime los elememntos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int arreglo[], int tamanio){
	
	printf(" = { "); 
	
	for(int i=0;i<(tamanio-1);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	
	printf("%d }",arreglo[(tamanio-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
//Este procedimiento permite buscar un entero dentro de un arreglo mediante busqueda binaria e imprimir en pantalla cuantas veces este se repite y sus posiciones
// En caso de no encontrar el valor emite un mensaje de que no lo encontro
void busqueda_binaria (int num_buscado,int arreglo[], int tamanio, int posiciones[]){
	int izq, der, centro, encontrados=0;
	izq = 0;
	der = tamanio - 1;
	centro = (izq+der)/2;
	while ((arreglo[centro] != num_buscado) && (izq < der)) {
		
		if (arreglo [centro] < num_buscado) {
			izq = centro + 1 ;
		}
		else {
			der = centro - 1;
		}
		
		centro = (izq + der) / 2;
	}
	
	if (num_buscado == arreglo [centro]) {
		
		encontrados=1;
		posiciones[0]=centro;
		
		if( (num_buscado == arreglo[centro-1]) && (num_buscado == arreglo[centro+1])){
			
			int i=centro-1;
			int j= centro+1;
			
			while(arreglo[i]==num_buscado){
				
				posiciones[encontrados] = i;
				encontrados++;
				i--;
				
			}
			
			while(arreglo[j]==num_buscado){
				
				posiciones[encontrados] = j;
				encontrados++;
				j++;
				
			}
			
		}
		else if( num_buscado == arreglo[centro+1] ){
			
			int j= centro+1;
			
			while(arreglo[j]==num_buscado){
				
				posiciones[encontrados] = j;
				encontrados++;
				j++;
				
			}
		}
		else if( num_buscado == arreglo[centro-1] ){
			
			int i=centro-1;
			
			while(arreglo[i]==num_buscado){
				
				posiciones[encontrados] = i;
				encontrados++;
				i--;
				
			}
			
		}
		
		if(encontrados==1) {
			
			printf("El valor %d se encuentra solo 1 vez en el arreglo y se encuentra en la posici�n: %d.",num_buscado,posiciones[0]);
		
		}
		else
		{
			printf("El valor %d se encuentra repetido %d veces y se encuentra en las posiciones: ",num_buscado,encontrados);
			
			for(int k=0; k<encontrados-1;k++){
				
				printf("%d, ",posiciones[k]);
				
			}
			
			printf("%d.", posiciones[encontrados-1]);
			
		}
	}
	else {
		printf("No se encuentra el valor %d dentro del arreglo",num_buscado);
	}
}
