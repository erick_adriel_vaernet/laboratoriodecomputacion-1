#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int arreglo[], int tamanio);
void buscar_arreglo (int num_buscado,int arreglo[], int tamanio,int posiciones[]);

//Este programa imprime un arreglo A en pantalla, el usuario puede elegir un numero a buscar dentro del arreglo
//el programa buscara el n�mero y en caso de que est�, le dira en que posicion se encuentra
int main() {
	int A[15] = { 5,4,10,8,2,11,9,1,5,11,1 };
	int posiciones[11];
	int n_buscado;
	
	printf("A");
	imprimir_arreglo(A,10);// Se imprime el arreglo hasta la posici�n 10 (�ltima posicion con un elemento definido)
	
	//Se pide al usuario un n�mero para buscarlo en el arreglo
	printf("\nIngrese el n�mero que desea buscar en el arreglo:\n");
	scanf("%d",&n_buscado);
	
	//Se pasan como parametros al procedimiento buscar_arreglo: el numero buscado, el arreglo donde se va a buscar, el rango en el que se buscara (de 0 hasta el numero indicado), y un arreglo auxiliar para guardar esas posiciones
	buscar_arreglo(n_buscado,A,11,posiciones);
	
	getch();
	return 0;
}

//Este procedimiento imprime los elememntos de un arreglo de enteros dentro de corchetes {}
void imprimir_arreglo (int arreglo[], int tamanio){
	
	printf(" = { "); 
	
	for(int i=0;i<(tamanio-1);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	
	printf("%d }",arreglo[(tamanio-1)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}
	
//Este procedimiento permite buscar un entero dentro de un arreglo e imprimir en pantalla cuantas veces este se repite y sus posiciones
// En caso de no encontrar el valor emite un mensaje de que no lo encontro

void buscar_arreglo (int num_buscado,int arreglo[], int tamanio,int posiciones[]){
	int i,encontrados=0;
	
	for(i=0; i<tamanio;i++){
		if(arreglo[i]==num_buscado){
			
			posiciones[encontrados] = i;
			encontrados++;
		}
		
	}
	
	
	if(encontrados==1){
		
		printf("\nSe encontro el vlaor %d en la posicion: %d",num_buscado,posiciones[0]);
		
	}
	else if(encontrados>1){
		
		printf("\nSe encontraron %d elementos con el valor %d en el arreglo, estos se encuentran en las posiciones: ",encontrados,num_buscado);
		
		for(int j=0; j<encontrados-1;j++){
			
			printf("%d, ",posiciones[j]);
			
		}
		
		printf("%d.", posiciones[encontrados-1]);
	}
	else{
		printf("No se encontraron elementos con el valor buscado dentro del arreglo");
	}
}
	
	
