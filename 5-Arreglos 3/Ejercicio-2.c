#include <stdio.h>
#include <conio.h>
	
	void imprimir_arreglo (int arreglo[], int tamanio);
int obtener_ult_posic(int arreglo[], int tamanio);
void ingresar_arreglo(int numero,int ult_posic, int arreglo [], int tamanio );		

//Programa que pide al usuario ingresar 1 numero y que luego insertaeste en un arreglo A ya definido y ordenado de menor a mayor;
int main() {
	int A [15]= {1,4,6,6,8,10,11,11,15,16,16};
	int n;
	int ult_posicion = obtener_ult_posic(A,15); //Se obtiene con esta funci�n la �ltima posici�n ocupada de un arreglo ordenado de menor a mayor
	
	printf("Arreglo A");
	imprimir_arreglo(A,ult_posicion);// Se pasa el arreglo y su ultima posici�n por parametros, la funci�n imprime los elementos del arreglo hasta la posicion indicada;
	
	//Se pide ingresar un n�mero y luego se pasa este como parametro a la funci�n ingresar_arreglo;
	printf("\n\nIngrese un entero para incluir en el arreglo :");
	scanf("%d",&n);
	
	// Esta funci�n ingresa el n�mero deseado en el arreglo segun el orden de menor a mayor ya establecido;
	ingresar_arreglo( n, ult_posicion, A, 15); // parametros: 1-numero a ingresar; 2-Ultima posici�n ocupada en el arreglo o N� de posiciones ocupadas; 3-El arreglo; 4-Tama�o de arreglo; 
	
	//Tras la insercion se aumenta en 1 la ultima posicion
	ult_posicion += 1;
	
	printf("Arreglo A");
	imprimir_arreglo(A , ult_posicion ); //Se imprime el arreglo en pantalla hasta el ultimo elemento;
	
	
	
	getch();
	return 0;
}

//Este procedimiento imprime los elememntos de un arreglo de enteros desde la posicion 0 hasta la indicada
void imprimir_arreglo (int arreglo[], int posic_final){
	
	printf(" = { "); 
	
	for(int i=0;i<(posic_final);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	
	printf("%d }",arreglo[(posic_final)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}

//Funci�n que permite obtener la �ltima posici�n ocupada en un arreglo ordenado de menor a mayor
int obtener_ult_posic(int arreglo[], int tamanio){
	int i;
	for(i=0; i<tamanio; i++){
		if((arreglo[i]!=0) && (arreglo[i+1]==0))return i; //Si no hay problemas inesperados siempre retorna la ultima posici�n del arreglo
	}
	
	printf("Error al obtener ultima posicion");//En caso de un fallo se manda un mensaje de error y se devuelve -1
	return -1;
}
	
//Permite ingresar un n�mero a un arreglo dado (solo para arreglos ordenados de menor a mayor)
void ingresar_arreglo(int numero,int ult_posic, int arreglo [], int tamanio ){ 
	
	int i, j;
	
	if(ult_posic == tamanio-1){
		printf("*Advertencia*: No quedan posiciones libres en el arreglo para insertar un nuevo elemento. Puede insertar si desea un nuevo elemento pero se reemplazaran valores anteriores");
	}
	for( i = 0; i <=ult_posic; i++ ){
		
		if( numero <= arreglo[i] ){
			
			for( j = ult_posic ; j >= i; j-- ){
				
				arreglo[j+1] = arreglo [j];
			}
			
			arreglo[i] = numero;
			break;
		}
	}
	
	if( numero > arreglo[ult_posic] ){
		arreglo[ult_posic+1] = numero;
		
	}
}
	
	
	
