#include <stdio.h>
#include <conio.h>

void imprimir_arreglo (int arreglo[], int tamanio);

//Programa que pide al usuario ingresar 1 numero y que luego insertaeste en un arreglo A ya definido
int main() {
	int A [15]= {5,4,10,8,2,11,9,1};
	int n;
	int ult_posicion = 7;
	
	printf("Arreglo A");
	imprimir_arreglo(A,ult_posicion);// Se pasa el arreglo y su ultima posici�n por parametros, la funci�n imprime los elementos del arreglo hasta la posicion indicada;
	
	//Se pide ingresar un n�mero. Al ingresarlo este se lo guarda en la variable n
	printf("\n\nIngrese un entero para incluir en el arreglo :");
	scanf("%d",&n);
	
	A[ult_posicion+1] = n; //Se inserta el numero ingresado en la ultima posici�n del arreglo
	
	//Tras la insercion se aumenta en 1 la ultima posici�n
	ult_posicion += 1;
	
	printf("Arreglo A");
	imprimir_arreglo(A , ult_posicion ); //Se imprime el arreglo en pantalla hasta el ultimo elemento;
	
	getch();
	return 0;
}

//Este procedimiento imprime los elememntos de un arreglo de enteros desde la posicion 0 hasta la indicada
void imprimir_arreglo (int arreglo[], int posic_final){
	
	printf(" = { "); 
	
	for(int i=0;i<(posic_final);i++)
	{
		printf("%d, ",arreglo[i]); // En cada iteracion se imprime los diferentes valores dentro del arreglo separados por coma, excepro el utimo valor
	}
	
	printf("%d }",arreglo[(posic_final)]); // Se imprime el ultimo valor sin coma al final, junto con el cierre del corchete
	
}	
