#include <stdio.h>
#include <conio.h>

float matrizSolucion[3][4];
void imprimir_matriz(float Matriz[3][4]);
void matriz_solucion(float matriz[3][4]);

int main(int argc, char *argv[]) {
	
	float matriz_1[3][4] = {{2, 1, -1, 1}, {1, 3, -4, 2}, {2, 6, -8, 4}};
	float matriz_2[3][4] = {{4, -2, -3, 5}, {-8, -1, 1, -5}, {2, 1, 2, 5}};
	
	printf("*****Resoluci�n de ejercicios a y b del punto 5 de la gu�a de matem�tica*****\n");
	printf("\nErjercicio A:\n");
	printf("2x + y - z = 1\n");
	printf("x + 3y - 4z = 2\n");
	printf("2x + 6y - 8z = 4\n");
	printf("\n\n Matriz:\n");
	imprimir_matriz(matriz_1);
	matriz_solucion( matriz_1);
	
	printf("\nErjercicio B:\n");
	printf("4x - 2y - 3z = 5\n");
	printf("-8x - 1y + 1z = -5\n");
	printf("2x + 1y + 2z = 5\n");
	printf("\n\n Matriz:\n");
	imprimir_matriz(matriz_2);
	matriz_solucion( matriz_2);
	
	getch();
	return 0;
}

void imprimir_matriz(float Matriz[3][4])
{
	int i, j;
	for (i=0; i<3; i++)
	{
		for (j=0; j<4; j++)
		{
			printf("[%.0f]\t", Matriz[i][j]);
		}
		printf("\n");
	}
}

void matriz_solucion( float matriz[3][4])
{
	int i, j,n=0;
	for (i=0; i<3; i++)
	{
		for (j=0; j<4; j++)
		{
			if(i==0) matrizSolucion[i][j] = matriz[i][j];
			else{
				switch(j){
				case 0: matrizSolucion[i][j]=0;
				break;
				case 1:  matrizSolucion[i][j]=(matriz[0][0]*matriz[i][j])-(matriz[0][j]*matriz[i][0]);
				break;
				case 2: matrizSolucion[i][j]=(matriz[0][0]*matriz[i][j])-(matriz[0][j]*matriz[i][0]);
				break;
				case 3:  matrizSolucion[i][j]=(matriz[0][0]*matriz[i][j])-(matriz[0][j]*matriz[i][0]);
				break;
				default: printf("\n**Error en matriz_soluci�n switch-1**\n");
				break;
				}
			}			
			
		}
	}
	i=2;
	for (j=0; j<4; j++)
	{
		switch(j){
		case 0: matrizSolucion[i][j]=0;
		break;
		case 1:  matrizSolucion[i][j]=0;
		break;
		case 2: matrizSolucion[i][j]=(matriz[1][1]*matriz[i][j])-(matriz[1][j]*matriz[i][1]);
		break;
		case 3:  matrizSolucion[i][j]=(matriz[1][1]*matriz[i][j])-(matriz[1][j]*matriz[i][1]);
		break;
		default: printf("\n**Error en matriz_soluci�n switch-2**\n");
		break;
		}
	}
	

	printf("\nMatriz ampliada:\n");
	imprimir_matriz(matrizSolucion);
	for(j=0; j<4; j++)
	{
		if(matrizSolucion[2][j]!=0)n++;
	}
	switch(n){
	case 0: printf("\n***Sistema Compatible Indeterminado***");
	break;
	case 1:if(matrizSolucion[2][2]==0) printf("\n***Sistema Incompatible***");
		else if((matrizSolucion[2][2]!=0))
		{
			printf("\n***Sistema Compatible Determinado***");
			float x,y,z;
			z = matrizSolucion[2][3]/matrizSolucion[2][2];
			y = (matrizSolucion[1][3]-matrizSolucion[1][2]*z)/ matrizSolucion[1][1];
			x = (matrizSolucion[0][3] - matrizSolucion[0][2]*z - matrizSolucion[0][1]*y)/matrizSolucion[0][0];
			printf("\n Solucion: x=%.2f, y=%.2f, z=%.2f",x,y,z);
		}	
	break;
	case 2: printf("\n***Sistema Compatible Determinado***");
			float x,y,z;
			z = matrizSolucion[2][3]/matrizSolucion[2][2];
			y = (matrizSolucion[1][3]-matrizSolucion[1][2]*z)/ matrizSolucion[1][1];
			x = (matrizSolucion[0][3] - matrizSolucion[0][2]*z - matrizSolucion[0][1]*y)/matrizSolucion[0][0];
			printf("\n Solucion: x=%.2f, y=%.2f, z=%.2f",x,y,z);
	break;
	default:printf("\n**Error en matriz_soluci�n switch-3**\n");
	break;
	}
}
