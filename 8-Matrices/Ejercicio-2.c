#include <stdio.h>
#include <conio.h>

int matrizAux[3][3];

void cargar_matriz (int matriz [3][3], int filas, int columnas);
int matrices_iguales (int matriz1 [3][3], int matriz2[3][3], int filas, int columnas);
void mostrar_matriz(int matriz [3][3], int filas, int columnas);
void suma_matrices(int matriz1 [3][3], int matriz2[3][3], int filas, int columnas);
void resta_matrices(int matriz1 [3][3], int matriz2[3][3], int filas, int columnas);
void producto_matrices(int matriz1 [3][3], int matriz2[3][3], int filas, int columnas);

int main(int argc, char *argv[]) {
	int M[3][3];
	int N[3][3];
	int igualdad;
	
	printf("\nCargue los elementos de la 1er matriz (denominada 'M'):\n");
	cargar_matriz(M,3,3);
	
	printf("\nCargue los elementos de la 2da matriz (denominada 'N'):\n");
	cargar_matriz(N,3,3);
	
	igualdad= matrices_iguales(M,N,3,3);
	if(igualdad==1)printf("\n***Las matrices ingresadas son iguales***\n");
	else printf("\n***Las matrices ingresadas son diferentes***\n");
	printf("\nMatriz M:\n");
	mostrar_matriz(M,3,3);
	printf("\nMatriz N:\n");
	mostrar_matriz(N,3,3);
	
	suma_matrices(M,N,3,3);
	
	resta_matrices(M,N,3,3);
	
	producto_matrices(M,N,3,3);
	
	getch();
	return 0;
}

void cargar_matriz(int matriz [3][3], int filas, int columnas)
{
	int i,j;
	int elemMatriz;
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			printf("Ingrese el elemento [%d][%d] de la matriz (Debe de ser un n�mero entero):",i,j);
			scanf("%d",&elemMatriz);
			matriz[i][j]=elemMatriz;
			
		}
	}
}

int matrices_iguales(int matriz1 [3][3], int matriz2[3][3], int filas, int columnas)
{
	int i,j;
	int check=0;
	int elemTotales= filas*columnas;
	
	for(i=0; i<filas; i++)
	{
		for(j=0;j<columnas;j++)
		{
			if(matriz1[i][j] == matriz2[i][j])check++;
		}
	}
	
	if(check==elemTotales)return 1;
	else return 0;
}
void mostrar_matriz(int matriz [3][3], int filas, int columnas)
{
	int i,j;
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			printf(" %d ",matriz[i][j]);
		}
		printf("\n");
	}
}

void suma_matrices(int matriz1 [3][3], int matriz2[3][3], int filas, int columnas)
{
	int i,j;
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			matrizAux[i][j] =matriz1[i][j] + matriz2[i][j];
		}
	}
	printf("\nLa suma de las matrices anteriores da la siguiente matriz: \n");
	mostrar_matriz(matrizAux,3,3);
}

void resta_matrices(int matriz1 [3][3], int matriz2[3][3], int filas, int columnas)
{
	int i,j;
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			matrizAux[i][j] =matriz1[i][j] - matriz2[i][j];
		}
	}
	printf("\nLa resta de las matrices anteriores da la siguiente matriz: \n");
	mostrar_matriz(matrizAux,3,3);
}
void producto_matrices(int matriz1 [3][3], int matriz2[3][3], int filas, int columnas)
{
	int i,j,n;
	int acum=0;
	
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			n=0;
			while(n<3)
			{
				acum +=(matriz1[i][n] * matriz2[n][j]);
				n++;
			}			
			matrizAux[i][j] = acum;
		}
	}
	
	printf("\nEl producto de las matrices anteriores ( M*N ) da la siguiente matriz: \n");
	mostrar_matriz(matrizAux,3,3);
}
