#include <stdio.h>
#include <conio.h>


int cargar_matriz(int matriz [4][4], int filas, int columnas);
int mostrar_matriz(int matriz [][4], int filas, int columnas);
int sumatoria_matriz(int matriz [4][4], int filas, int columnas);
double promedio_matriz(int sumatoria, int filas, int columnas);
int busqueda_matriz(int matriz [4][4], int filas, int columnas);
int matriz_traspuesta( int matriz [][4], int filas, int columnas);
int pares_matriz(int matriz [4][4], int filas, int columnas);
int primos_matriz(int matriz [4][4], int filas, int columnas);

int main(int argc, char *argv[]) {
	
	int aux;
	double promedio;
	int M [4][4];
	
	aux= cargar_matriz(M,4,4);
	aux= mostrar_matriz(M,4,4);
	
	aux= sumatoria_matriz(M,4,4);
	printf("\nLa sumatoria de los elementos de la matriz es de %d \n",aux);
	
	promedio = promedio_matriz(aux,4,4);
	printf("El promedio obtenido de la matriz es de %.2f \n", promedio);
	
	aux= busqueda_matriz(M,4,4);
	
	aux= matriz_traspuesta( M,4,4);
	printf("Matriz traspuesta:\n");
	aux= mostrar_matriz(M,4,4);
	
	aux= pares_matriz(M,4,4);
	printf("La cantida de numeros pares en la amtriz es de %d \n", aux);
	
	aux= primos_matriz(M,4,4);
	
	return 0;
}

int cargar_matriz(int matriz [4][4], int filas, int columnas)
{
	int i,j;
	int elemMatriz;
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			printf("Ingrese el elemento [%d][%d] de la matriz (Debe de ser un n�mero entero):",i,j);
			scanf("%d",&elemMatriz);
			matriz[i][j]=elemMatriz;
			
		}
	}
	return 0;
}

int mostrar_matriz(int matriz [][4], int filas, int columnas)
{
	int i,j;
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			printf(" %d ",matriz[i][j]);
		}
		printf("\n");
	}
	return 0;
}

int sumatoria_matriz(int matriz [4][4], int filas, int columnas)
{
	int i,j;
	int sumatoria=0;
	
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			sumatoria+= matriz[i][j];
		}
		printf("\n");
	}
	
	return sumatoria;
}


double promedio_matriz(int sumatoria, int filas, int columnas)
{
	double sum = sumatoria, f=filas, c=columnas;
	return (sum / (f*c));
}

int busqueda_matriz(int matriz [4][4], int filas, int columnas)
{
	int i,j;
	int nBuscado;
	printf("Ingrese el elemento que desea buscar en la matriz (Debe de ser un n�mero entero): ");
	scanf("%d",&nBuscado);
	
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			if(matriz[i][j]==nBuscado)
			{
				printf("El numero buscado se encuentra en la posicion [%d][%d]\n",i,j);
				return 0;
			}
		}
	}
	printf("El numero buscado no se encuentra en la matriz\n");
	return -1;
}

int matriz_traspuesta(int matriz [][4], int filas, int columnas)
{
	int i,j, auxiliar;
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			auxiliar= matriz[i][j];
			matriz[i][j]=matriz[j][i];
			matriz[j][i]= auxiliar;
		}
	}
	return 0;
}

int pares_matriz(int matriz [4][4], int filas, int columnas)
{
	int i,j;
	int pares=0;
	
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			if(matriz[i][j] % 2 == 0) pares++;
		}
	}
	return pares;
}

int primos_matriz(int matriz [4][4], int filas, int columnas)
{
	int i,j;
	int primos=0;
	
	printf("Numeros primos dentro de la matriz:\n");
	for(i=0;i<filas; i++)
	{
		for(j=0; j<columnas; j++)
		{
			if(matriz[i][j] %2 != 0 && matriz[i][j] %3 != 0)
			{
				printf(", %d ", matriz[i][j]);
				primos++;
			}
		}
	}
	if(primos==0) printf("No existen numeros primos en esta matriz");
	return 0;
}
