//Esta función permite buscar un entero dentro de un arreglo con busqueda binaria y devolver la posicion donde se encuentra, en caso de no encontrarlo retornara -1
int busqueda_binaria (int arreglo [], int tamanio, int num_buscado) {
	int izq, der, centro;
	izq = 0;
	der = tamanio - 1;
	centro = (izq+der)/2;
	while ((arreglo[centro] != num_buscado) && (izq < der)) {
		if (arreglo [centro] < num_buscado) {
			izq = centro + 1 ;
		}
		else {
			der = centro - 1;
		}
		centro = (izq + der) / 2;
	}
	if (num_buscado == arreglo [centro]) {
		return (centro);
	}
	else {
		return (- 1);
	}
}