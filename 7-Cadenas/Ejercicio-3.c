#include <stdio.h>
#include <conio.h>
#include <string.h>

/*Ejercicio N� 3:
En una cadena de 20 caracteres, cargar dos nombres separados por puntos. Usar la funci�n strlen para saber la cantidad de caracteres que tiene la cadena y completar el resto con el caracter "a."
*/

int main()
{
	char cadena[20];	
	int longitud;
	int i;
	
	/*Cargamos la Cadena*/
	printf("Ingrese 2 nombres separados por '.' : \n");
	scanf("%s",&cadena);
	
	//Obtenemos la longitud de la cadena con la funci�n strlen
	longitud = strlen(cadena);
	
	//LLenamos el resto de los espacios con el caracter 'a'
	for(i=longitud; i<(20); i++)
	{
		cadena[i]='a';
	}
	
	//Imprimimos la cadena llenada con el caracter 'a'
	printf("\nEspacios en blancos llenados con 'a':\n %s",cadena);

	getch();
	return 0;
}

