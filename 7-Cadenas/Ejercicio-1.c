#include <stdio.h>
#include <string.h>
#include <conio.h>

/*Ejercicio N�1: Ingresar una cadena por teclado. Se pide
"	Mostrar la cadena
"	Mostrar la cadena al rev�s.
"	Indique cuantas veces aparece una vocal que se ingresa por teclado.
"	Indicar cuantas veces aparece la siguiente secuencia de caracteres: "ca"
"	Indicar si la cadena es un pal�ndromo
*/

int contar_vocales(char caracter,char cadena[]);
int es_palindromo(char palabra[], int tamanio);

int main(int argc, char *argv[]) {
	
	char car;
	char palabra [50];
	int i=0,j;
	int vocales,ca=0;
	int palindromo;
	
	printf("Ingrese una palabra:");
	scanf("%s",palabra);
	
	while(palabra[i]!='\0')
	{
		printf("\n");
		printf("%c", palabra[i]);
		i++;
	}
	
	printf("\nCadena al revez:");
	
	j=i-1;
	while(j>=0)
	{
		printf("\n");
		printf("%c", palabra[j]);
		j--;
	}
	
	printf("\nIngrese una vocal: ");
	scanf(" %c",&car);
	vocales = contar_vocales(car,palabra);
	if(vocales>0) printf("La cantidad de veces que aparece esta vocal es de:%d",vocales);
	else if(vocales==0) printf("La vocal no aparece ni una vez en el texto");
	else if(vocales<0)printf("El caracter ingresado no es una vocal");
	
	j=0;
	while(j<i)
	{
		if(palabra[j]=='c'&&palabra[j+1]=='a')ca++;
		j++;
	}
	
	printf("\nLa cantidad de veces que a parece la secuencia 'ca' es de: %d",ca);
	
	palindromo= es_palindromo(palabra,i);
	printf("\nLa secuencia ingresada");
	if(palindromo==1)printf(" es un palindromo");
	else printf(" NO es un palindromo");

	getch();
	return 0;
}

int contar_vocales(char caracter,char cadena[])
{
	int i=0, vocales=0;
	if(caracter=='a'||caracter=='e'||caracter=='i'||caracter=='o'||caracter=='u')
	{
		while(cadena[i]!='\0'){
			if(cadena[i]==caracter) vocales++;
			i++;
		}
	}else return-1;
	
	return vocales;
}

int es_palindromo(char palabra[], int tamanio)
{
	int i=0, p=0;
	int tam=tamanio-1;
	int mitad= tam/2;
	
	if(tamanio%2==0)
	{
		while(i <= mitad)
		{
			if(palabra[i]==palabra[tam-i]) p++;
			i++;
		}
	}
	else
	{
		while(i < mitad)
		{
			if(palabra[i]==palabra[tam-i]) p++;
			i++;
		}
	}
	
	
	if (p == tamanio/2) return 1;
	else return 0;
	
}



