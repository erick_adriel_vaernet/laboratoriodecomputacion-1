#include <stdio.h>
#include <string.h>
#include <conio.h>

/*Ejercicio 2:	Ingresar por teclado  un texto que contenga  oraciones y p�rrafos. Cada oraci�n est� separada por el car�cter punto y cada p�rrafo est� separado por el car�cter asterisco. Se pide;
o	Mostrar por pantalla cada p�rrafo separado por una l�nea en blanco.
	o	Indicar cuantas oraciones tiene cada p�rrafo, total de p�rrafos y total de oraciones.
	o	Mostrar por pantalla la primera oraci�n de cada p�rrafo indicando a que p�rrafo pertenece.
	o	Indicar cu�l es el p�rrafo que tiene m�s oraciones y mostrarlo por pantalla. Cada oraciones debe aparecer una debajo de la otra.
	*/

void imprimir_parrafos_y_contar(char text[]);
void posic_parrafos(int parrafo, int i);
void contar_oraciones_parrafo(int parrafo);
void imprimir_oracion(char text[],int posicInicio);
int mayor_parrafo();
void imprimir_parrafos(char text[],int inicio);

int p=1;//p es la cantidad de parrafos	
int finParrafos[5]; // arreglo para guardar la posici�n donde termina cada parrafo
int oracionesPorP[5]={0,0,0,0,0}; // arreglo para guardar la cantidad de oraciones por cada parrafo


int main(int argc, char *argv[]) {
	char texto[300];
	int parrafos, oraciones,parrafoMasLargo;
	
	printf("Ingrese oraciones separadas por puntos y parrafos separados por asteriscos:\n");
	scanf("%[^\n]",texto);
	fflush(stdin);
	printf("\n*Parrafos separados por linea en blanco:\n");
	
	imprimir_parrafos_y_contar(texto);//Imprime parrafos separados por\n + cuenta parrafos en 'p' + cuenta oracion x parrafo + guarda en finParrafos[] el fin de cada parrrafo
	
	parrafos = p;
	oraciones =oracionesPorP[0]+oracionesPorP[1]+oracionesPorP[2]+oracionesPorP[3]+oracionesPorP[4];

	printf("*Cantidad de parrafos:%d",parrafos);	
	printf("\n*Cantidad de oraciones:%d",oraciones);
	
	printf("\n*Cantidad de oraciones por parrafo:");
	if(p>1)printf("\n-Parrafo 1: %d oraciones",oracionesPorP[0]);
	if(p>2)printf("\n-Parrafo 2: %d oraciones",oracionesPorP[1]);
	if(p>3)printf("\n-Parrafo 3: %d oraciones",oracionesPorP[2]);
	if(p>4)printf("\n-Parrafo 4: %d oraciones",oracionesPorP[3]);
	if(p>5)printf("\n-Parrafo 5: %d oraciones",oracionesPorP[4]);
	
	printf("\n\n*Primera oraci�n de cada parrafo:");
	if(p>1)
	{
		printf("\n-Parrafo 1: ");
		imprimir_oracion(texto,0);
	}
	if(p>2)
	{
		printf("\n-Parrafo 2: ");
		imprimir_oracion(texto,finParrafos[0]);
	}
	if(p>3)
	{
		printf("\n-Parrafo 3: ");
		imprimir_oracion(texto,finParrafos[1]);
	}
	if(p>4)
	{
		printf("\n-Parrafo 4: ");
		imprimir_oracion(texto,finParrafos[2]);
	}
	if(p>5)
	{
		printf("\n-Parrafo 5: ");
		imprimir_oracion(texto,finParrafos[3]);
	}
	
	parrafoMasLargo = mayor_parrafo();
	printf("\n\n*El parrafo con mayor cantidad de oraciones es el %d",parrafoMasLargo);
	printf("\n*El parrafo consta de las siguientes oraciones:\n ");
	imprimir_parrafos(texto,finParrafos[parrafoMasLargo-2]);//Se resta 2 a parrafoMasLargo debido a: -1 por la posicion del arreglo comienza en 0 no en 1, y otro -1 porque se pasa la posicion de fin del parrafo anterior al que se quiere imprimir
	
	getch();
	return 0;
}



/*Procedimiento que imprime los diferentes parrafos de un texto dado por espacios en blanco, a su vez aprovecha el bucle y cuenta las oraciones por parrafo guardandolas
en la variable global 'p'; cunta la cantidad de parrafos y las posiciones del final de cada parrafo*/
void imprimir_parrafos_y_contar(char text[])//parametros: arreglo con el texto y la cantidad de caracteres
{
	int i=0;
	while(text[i]!='\000')
	{
		if(text[i]=='.')
		{
			contar_oraciones_parrafo(p); //Funci�n que cuenta las oraciones por parrafo y las guarda en el arreglo oracionesPorP[]
		}
		if(text[i]=='*')
		{
			posic_parrafos(p,i); //Funci�n que guarda posiciones del final de parrafo en el arreglo finParrafos[]
			p++;
			printf("\n\n");//si se encuentra un asterisco se salta un renglon
			i++;
			continue;//y se saltea el printf siguiente debido a que este imprimiria un '*'
		}
		
		printf("%c",text[i]);//Se imprimen los caracteres del texto
		i++;
	}
}

//Procedimiento que guarda en un arrreglo finParrafos[], las diferentes posiciones donde termina cada parrafo
void posic_parrafos(int parrafo,int i)
{
	switch(parrafo)
	{
	case 1: finParrafos[0]= i;
		break;
	case 2: finParrafos[1]= i;
		break;
	case 3: finParrafos[2]= i;
		break;
	case 4: finParrafos[3]= i;
		break;
	case 5: finParrafos[4]= i;
		break;
	default: printf("\n**Error**: Este programa soporta solo hasta 5 parrafos\n");
		break;
	}
}

//Procedimiento que guarda la cantidad de oraciones en cada parrafo 
void contar_oraciones_parrafo(int parrafo)
{
	switch(parrafo)
	{
		
	case 1: oracionesPorP[0]++;
		break;
	case 2: oracionesPorP[1]++;
		break;
	case 3: oracionesPorP[2]++;
		break;
	case 4: oracionesPorP[3]++;
		break;
	case 5: oracionesPorP[4]++;
		break;
	default: printf("\n**Error en contar las oraciones por parrafo**\n");
		break;
	}
	
}

//Procedimiento que imprime la primera oraci�n de un parrafo dado
void imprimir_oracion(char text[],int posicInicio)
{
	int i;
	if(posicInicio==0) i=posicInicio;
	else i=posicInicio+1; //Para saltear el '*' al inicio de la oraci�n
	while(text[i]!='.')
	{
		printf("%c",text[i]);
		i++;
	}
}
//funci�n que devuelve el parrafo con mayor cantidad de oraciones
int mayor_parrafo()
{
	int i;
	int mayor=oracionesPorP[0];
	
	for(i=1;i<=4;i++)
	{
		if(oracionesPorP[i]>mayor) mayor=i;
	}
	return mayor+1;
}

//Procedimiento que imprime las otraciones de un parrafo separadas por un \n. Sus parametros son el arreglo con el texto y la posiciond e inicio del parrafo en cuestion o fin del parrafo anterior
void imprimir_parrafos(char text[],int inicio)
{
	int i=inicio+1;//Esto evita la posicion de inicio con '*' haciendo que inicie en la siguiente posici�n
	
	while(text[i]!='*')
	{
		printf("%c",text[i]);
		
		if(text[i]=='.') printf("\n");
		i++;
	}
}

