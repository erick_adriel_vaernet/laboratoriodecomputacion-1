#include <stdio.h>
#include <conio.h>
#include <string.h>

int cargar_palabras(char lista[][50],int ultimo);
void ordenar_alfabeticamente(char lista[][50],int ultimo);
void imprimir_lista_strings(char lista[][50],int ultimo);

//Este programa carga un a lista de nombres dados por el usuario, los ordena alfabeticamente y los vuelve a imprimir en pantalla
int main(int argc, char *argv[]) {
	
	char linea[100][50];
	int ultimo_elem;
	
	printf("\nIntroducir debajo cada cadena en una linea:");
	printf("\n\nEscribir 'FIN' para terminar:\n\n");
	
	ultimo_elem = cargar_palabras(linea,100);

	printf("\nLista reordenada de cadenas:\n");
	
	ordenar_alfabeticamente(linea,ultimo_elem);
	
	imprimir_lista_strings(linea,ultimo_elem);
	
	getch();
	return 0;
}

//Esta funcion carga una matriz con palabras,es decir guarda una lista de palabras y retorna el numero de la ultima fila donde se ingreso una palabra
int cargar_palabras(char lista[][50],int max){
	
	int i=0;
	
	do
	{
		printf("Cadena %i: ",i+1);
		gets(lista[i]);
		
		i++;
		
		if(i==max) break; // Se finaliza el cargado de palabras si el numero de las mismas llega a 100 (esto en caso de no introducir "fin" o "FIN")
		
	}while(strcmp(lista[i-1],"FIN")!=0 && strcmp(lista[i-1],"fin")!=0);
	
	return i-1;
}

//Procedimiento que ordena alfabeticamente una lista de palabras
void ordenar_alfabeticamente(char lista[][50],int ultimo){
	
	int i,j,k;
	char aux[50];
	
	for(i=0; i<ultimo-1; i++)
	{
		k=i;
		strcpy(aux, lista[i]);
		
		for(j=i+1; j<ultimo; j++)
		{
			if(strcmp(lista[j], aux)<0)
			{
				k=j;
				strcpy(aux, lista[j]);
				
			}
		}
		
		strcpy(lista[k],lista[i]);
		strcpy(lista[i],aux);
	}

}

//Procedimiento que imprime una lista de palabras 
void imprimir_lista_strings(char linea[][50],int ultimo){
	
	for(int i=0; i<ultimo ;i++)
	{
		printf("\nCadena %i: %s",i+1,linea[i]);	
	}
}
